package pl.lyskawa.finanteq.bindingadapters

import android.databinding.BindingAdapter
import android.widget.TextView

object TextViewBinding {
    @BindingAdapter("stringFormat", "stringResource")
    @JvmStatic
    fun bindStringFormatResource(view: TextView, format: Int, resource: Int) {
        view.context.let {
            view.text = it.getString(format, resource)
        }
    }

    @BindingAdapter("stringFormat", "stringResource")
    @JvmStatic
    fun bindStringFormatResource(view: TextView, format: String, resource: Int) {
        view.context.let {
            view.text = String.format(format, it.getString(resource))
        }
    }

}