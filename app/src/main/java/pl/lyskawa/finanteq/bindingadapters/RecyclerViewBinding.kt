package pl.lyskawa.finanteq.bindingadapters

import android.databinding.BindingAdapter
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView

object RecyclerViewBinding {

    @BindingAdapter("separator")
    @JvmStatic
    fun setSeparator(view: RecyclerView, orientation: Int) {
        view.addItemDecoration(DividerItemDecoration(view.context, orientation))
    }
}