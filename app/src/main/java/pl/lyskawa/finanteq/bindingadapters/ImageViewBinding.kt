package pl.lyskawa.finanteq.bindingadapters

import android.databinding.BindingAdapter
import android.support.v7.widget.AppCompatImageView
import android.widget.ImageView
import com.bumptech.glide.Glide

object ImageViewBinding {

    @BindingAdapter("srcCompat")
    @JvmStatic
    fun bindSrcCompatIntDrawable(view: AppCompatImageView, id: Int) {
        view.setImageDrawable(view.context.let { it.resources.getDrawable(id, it.theme) })
    }

    @BindingAdapter("android:src")
    @JvmStatic
    fun bindSrcUrl(view: ImageView, url: String) {
        Glide.with(view.context).load(url).into(view)
    }
}