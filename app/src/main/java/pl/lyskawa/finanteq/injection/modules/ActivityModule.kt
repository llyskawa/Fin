package pl.lyskawa.finanteq.injection.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.lyskawa.finanteq.ui.details.DetailsActivity
import pl.lyskawa.finanteq.ui.users.UsersActivity

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeUsersActivity(): UsersActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeDetailsActivity(): DetailsActivity
}