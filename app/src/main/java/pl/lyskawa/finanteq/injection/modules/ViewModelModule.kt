package pl.lyskawa.finanteq.injection.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.lyskawa.finanteq.ViewModelFactory
import pl.lyskawa.finanteq.injection.ViewModelKey
import pl.lyskawa.finanteq.ui.details.DetailsViewModel
import pl.lyskawa.finanteq.ui.users.UsersViewModel
import pl.lyskawa.finanteq.ui.users.fragment.UsersFragmentViewModel
import pl.lyskawa.finanteq.ui.users.list.UsersListViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(UsersViewModel::class)
    abstract fun bindUsersViewModel(usersViewModel: UsersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UsersFragmentViewModel::class)
    abstract fun bindUsersFragmentViewModel(usersFragmentViewModel: UsersFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UsersListViewModel::class)
    abstract fun bindUsersListViewModel(usersListViewModel: UsersListViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(detailsViewModel: DetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}