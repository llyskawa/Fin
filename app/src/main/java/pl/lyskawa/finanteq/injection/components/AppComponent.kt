package pl.lyskawa.finanteq.injection.components

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import pl.lyskawa.finanteq.FinanteqApp
import pl.lyskawa.finanteq.injection.modules.ActivityModule
import pl.lyskawa.finanteq.injection.modules.AppModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            AppModule::class,
            ActivityModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(finanteqApp: FinanteqApp)
}