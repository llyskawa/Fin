package pl.lyskawa.finanteq.injection.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.lyskawa.finanteq.ui.users.fragment.UsersFragment

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeUsersFragment(): UsersFragment
}