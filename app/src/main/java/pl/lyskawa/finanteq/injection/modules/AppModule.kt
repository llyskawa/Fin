package pl.lyskawa.finanteq.injection.modules

import dagger.Module
import dagger.Provides
import pl.lyskawa.finanteq.data.remote.DailyMotionApi
import pl.lyskawa.finanteq.data.remote.GithubApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideGithubService(): GithubApi {
        return Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubApi::class.java)

    }

    @Singleton
    @Provides
    fun provideDailyMotionService(): DailyMotionApi {
        return Retrofit.Builder()
                .baseUrl("https://api.dailymotion.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(DailyMotionApi::class.java)
    }
}