package pl.lyskawa.finanteq.extensions

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


fun <T : RecyclerView.ViewHolder, VH : RecyclerView.ViewHolder> RecyclerView.Adapter<VH>.viewHolder(viewGroup:
                                                                                                    ViewGroup,
                                                                                                    @LayoutRes layout: Int,
                                                                                                    action: (View) -> T): T {
    val view = LayoutInflater.from(viewGroup.context).inflate(layout, viewGroup, false)
    return action(view)
}