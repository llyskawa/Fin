package pl.lyskawa.finanteq.ui.users.list

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import pl.lyskawa.finanteq.R
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.extensions.viewHolder
import javax.inject.Inject

class UsersListAdapter
@Inject
constructor() : RecyclerView
.Adapter<UsersListViewHolder>() {
    private var items: List<User> = emptyList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersListViewHolder =
            viewHolder(parent, R.layout.item_user, ::UsersListViewHolder)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: UsersListViewHolder, position: Int) {
        holder.viewModel.update(items[position])
        holder.executePendingBindings()
    }

    fun updateData(items: List<User>?) {
        items?.let {
            this.items = items
            notifyDataSetChanged()
        }
    }

}