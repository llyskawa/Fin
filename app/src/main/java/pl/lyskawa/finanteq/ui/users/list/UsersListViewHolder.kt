package pl.lyskawa.finanteq.ui.users.list

import android.app.Activity
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v4.app.ActivityOptionsCompat
import android.view.View
import pl.lyskawa.finanteq.databinding.ItemUserBinding
import pl.lyskawa.finanteq.navigation.NavigationInformation
import pl.lyskawa.finanteq.ui.base.BaseFragmentViewHolder

class UsersListViewHolder(view: View) : BaseFragmentViewHolder<ItemUserBinding, UsersListViewModel>
(view) {
    init {
        viewModel = UsersListViewModel()
        val context = this.itemView.context
        viewModel.navigate.observe(context as LifecycleOwner, Observer<NavigationInformation> {
            it?.let {

                val transition = ActivityOptionsCompat.makeSceneTransitionAnimation(context as Activity,
                        android.support.v4.util.Pair(binding.circleImageView as View, binding
                                .circleImageView
                                .transitionName)).toBundle()

                val intent = Intent(context, it.activity).apply {
                    it.modifyIntent?.let {
                        it.invoke(this)
                    }
                }
                context.startActivity(intent, transition)
            }
        })
        bindContentView(view)
    }
}