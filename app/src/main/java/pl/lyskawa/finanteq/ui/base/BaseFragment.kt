package pl.lyskawa.finanteq.ui.base

import android.arch.lifecycle.ViewModel
import android.databinding.ViewDataBinding
import android.support.v4.app.Fragment

abstract class BaseFragment<B : ViewDataBinding, VM : ViewModel> : Fragment() {
    protected lateinit var binding: B
    protected abstract val viewModel: VM
}