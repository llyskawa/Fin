package pl.lyskawa.finanteq.ui.users.fragment

import android.arch.lifecycle.MutableLiveData
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.data.model.UserSource
import pl.lyskawa.finanteq.data.remote.DailyMotionApi
import pl.lyskawa.finanteq.data.remote.GithubApi
import pl.lyskawa.finanteq.data.repository.DailymotionRepository
import pl.lyskawa.finanteq.data.repository.GithubRepository
import pl.lyskawa.finanteq.ui.base.BaseViewModel
import javax.inject.Inject

class UsersFragmentViewModel
@Inject
constructor(val githubApi: GithubApi,
            val dailyMotionApi: DailyMotionApi) : BaseViewModel() {
    val users: MutableLiveData<List<User>> = MutableLiveData()
    val error: MutableLiveData<Throwable> = MutableLiveData()

    var loading: Boolean = false

    private var loadingGithub: Boolean = false
        set(value) {
            field = value
            if (!value && !loadingDailymotion) {
                loading = false
                notifyChange()
            }
        }

    private var loadingDailymotion: Boolean = false
        set(value) {
            field = value
            if (!value && !loadingGithub) {
                loading = false
                notifyChange()
            }
        }


    fun loadData() {
        loading = true
        users.value = emptyList()

        fetchGithub(GithubRepository(githubApi))
        fetchDailymotion(DailymotionRepository(dailyMotionApi))
    }

    private fun fetchGithub(githubRepository: GithubRepository) {
        loadingGithub = true
        githubRepository.getUsers {
            it.forEach { it.source = UserSource.GITHUB }
            onDataFetched(it, UserSource.GITHUB)
        }
    }

    private fun fetchDailymotion(dailymotionRepository: DailymotionRepository) {
        loadingDailymotion = true
        dailymotionRepository.getUsers {
            it.forEach { it.source = UserSource.DAILYMOTION }
            onDataFetched(it, UserSource.DAILYMOTION)
        }
    }

    fun onDataFetched(userList: List<User>, source: UserSource) {
        users.value = users.value?.plus(userList)
        when (source) {
            UserSource.GITHUB -> loadingGithub = false
            UserSource.DAILYMOTION -> loadingDailymotion = false
            UserSource.NONE -> throw IllegalArgumentException("${UserSource.NONE} is not an " +
                    "acceptable response")
        }
    }


}