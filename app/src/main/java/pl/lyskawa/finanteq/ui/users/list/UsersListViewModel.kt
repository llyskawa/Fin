package pl.lyskawa.finanteq.ui.users.list

import android.arch.lifecycle.MutableLiveData
import android.os.Bundle
import pl.lyskawa.finanteq.R
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.data.model.UserSource
import pl.lyskawa.finanteq.navigation.NavigationInformation
import pl.lyskawa.finanteq.ui.base.BaseViewModel
import pl.lyskawa.finanteq.ui.details.DetailsActivity
import javax.inject.Inject

class UsersListViewModel
@Inject
constructor() : BaseViewModel() {

    val navigate: MutableLiveData<NavigationInformation> = MutableLiveData()

    var username = ""
    var avatarUrl = ""
    var source = R.color.transparent

    lateinit var user: User

    fun update(user: User) {
        this.user = user
        username = user.username
        avatarUrl = user.avatarUrl
        source = when (user.source) {
            UserSource.DAILYMOTION -> R.drawable.ic_dailymotion
            UserSource.GITHUB -> R.drawable.ic_github
            else -> R.color.transparent
        }
        notifyChange()
    }

    fun onClick() {
        navigate.value = NavigationInformation(DetailsActivity::class.java) {
            putExtras(Bundle().apply { putParcelable(DetailsActivity.ARG_USER, user) })
        }
    }

}