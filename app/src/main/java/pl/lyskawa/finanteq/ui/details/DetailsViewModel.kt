package pl.lyskawa.finanteq.ui.details

import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.data.model.UserSource
import pl.lyskawa.finanteq.ui.base.BaseViewModel
import javax.inject.Inject

class DetailsViewModel @Inject constructor() : BaseViewModel() {

    var user: User? = null
    var source: Int = 0
        get() = when (user?.source) {
            UserSource.GITHUB -> pl.lyskawa.finanteq.R.string.user_source_github
            UserSource.DAILYMOTION -> pl.lyskawa.finanteq.R.string.user_source_dailymotion
            else -> 0
        }

    fun update(it: User) {
        user = it
        notifyChange()
    }
}