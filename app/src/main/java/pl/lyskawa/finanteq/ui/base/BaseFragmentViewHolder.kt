package pl.lyskawa.finanteq.ui.base

import android.arch.lifecycle.ViewModel
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import pl.lyskawa.finanteq.BR

abstract class BaseFragmentViewHolder<B : ViewDataBinding, VM : ViewModel>(itemView: View)
    : RecyclerView.ViewHolder(itemView) {
    protected lateinit var binding: B
    lateinit var viewModel: VM

    fun bindContentView(view: View) {
        binding = DataBindingUtil.bind(view)!!
        binding.setVariable(BR.vm, viewModel)
    }

    fun executePendingBindings() {
        binding.executePendingBindings()
    }
}