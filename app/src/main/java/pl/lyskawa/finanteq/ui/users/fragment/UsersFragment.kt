package pl.lyskawa.finanteq.ui.users.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_users.*
import pl.lyskawa.finanteq.R
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.databinding.FragmentUsersBinding
import pl.lyskawa.finanteq.injection.Injectable
import pl.lyskawa.finanteq.ui.users.list.UsersListAdapter
import timber.log.Timber
import javax.inject.Inject

class UsersFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var userAdapter: UsersListAdapter

    lateinit var viewModel: UsersFragmentViewModel
    lateinit var binding: FragmentUsersBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_users, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UsersFragmentViewModel::class.java]
        binding.vm = viewModel

        list.adapter = userAdapter


        viewModel.users.observe(this, Observer<List<User>> {
            userAdapter.updateData(it)
            userAdapter.notifyDataSetChanged()
        })

        viewModel.error.observe(this, Observer<Throwable> {
            Snackbar.make(binding.root, R.string.fetch_error, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.error_retry) {
                        viewModel.loadData()
                    }
                    .show()
            Timber.e("Error when fetching data", it)
        })

        viewModel.loadData()
    }
}