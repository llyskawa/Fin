package pl.lyskawa.finanteq.data.model

import com.google.gson.annotations.SerializedName

data class DailyMotionResponse(
        @SerializedName("page")
        val page:Int,
        @SerializedName("limit")
        val limit:Int,
        @SerializedName("has_more")
        val hasMore:Boolean,
        @SerializedName("list")
        val list:List<User>
)