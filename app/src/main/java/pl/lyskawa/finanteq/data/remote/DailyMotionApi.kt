package pl.lyskawa.finanteq.data.remote

import pl.lyskawa.finanteq.data.model.DailyMotionResponse
import retrofit2.Call
import retrofit2.http.GET

interface DailyMotionApi {
    @GET("/users?fields=avatar_360_url,username")
    fun getUsers(): Call<DailyMotionResponse>
}