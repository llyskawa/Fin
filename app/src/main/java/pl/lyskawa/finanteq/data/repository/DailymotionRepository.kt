package pl.lyskawa.finanteq.data.repository

import pl.lyskawa.finanteq.data.model.DailyMotionResponse
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.data.remote.DailyMotionApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class DailymotionRepository(val dailyMotionApi: DailyMotionApi) {
    fun getUsers(callback: (List<User>) -> Unit) {
        dailyMotionApi.getUsers().enqueue(object : Callback<DailyMotionResponse> {
            override fun onFailure(call: Call<DailyMotionResponse>?, t: Throwable?) {
                t?.let { throw t }
            }

            override fun onResponse(call: Call<DailyMotionResponse>?, response: Response<DailyMotionResponse>?) {
                response?.body()?.let {
                    callback(it.list)
                } ?: onFailure(call, IllegalArgumentException("Response or body is empty"))
            }
        })
    }
}