package pl.lyskawa.finanteq.data.model

enum class UserSource {
    NONE, GITHUB, DAILYMOTION
}