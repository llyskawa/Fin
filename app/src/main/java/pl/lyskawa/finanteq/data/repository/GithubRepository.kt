package pl.lyskawa.finanteq.data.repository

import pl.lyskawa.finanteq.data.model.GithubResponse
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.data.remote.GithubApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class GithubRepository(val githubApi: GithubApi) {
    fun getUsers(callback: (list: List<User>) -> Unit) {
        githubApi.getUsers().enqueue(object : Callback<GithubResponse> {
            override fun onFailure(call: Call<GithubResponse>?, t: Throwable?) {
                t?.let { throw it }
            }

            override fun onResponse(call: Call<GithubResponse>?, response: Response<GithubResponse>?) {
                response?.body()?.let {
                    callback(it)
                } ?: onFailure(call, IllegalArgumentException("Response or body is empty"))
            }
        })
    }
}