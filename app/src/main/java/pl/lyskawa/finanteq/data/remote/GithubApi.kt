package pl.lyskawa.finanteq.data.remote

import pl.lyskawa.finanteq.data.model.GithubResponse
import retrofit2.Call
import retrofit2.http.GET

interface GithubApi {
    @GET("/users")
    fun getUsers(): Call<GithubResponse>
}