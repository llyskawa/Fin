package pl.lyskawa.finanteq.navigation

import android.app.Activity
import android.content.Intent


data class NavigationInformation(val activity: Class<out Activity>,
                                 val modifyIntent: (Intent.() -> Unit)? = null)