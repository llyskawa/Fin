package pl.lyskawa.finanteq.data.remote

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

@RunWith(JUnit4::class)
class GithubApiTests {

    private lateinit var service: GithubApi

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubApi::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getUser_notNull() {
        mockResponse("GithubUsers.json")

        val users = service.getUsers().execute()

        assert(users.body() != null)
    }


    @Test
    fun getUser_notEmpty() {
        mockResponse("GithubUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!!.isNotEmpty())
    }

    @Test
    fun getUser_size() {
        mockResponse("GithubUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!!.size == 30) { "correct size is ${users.body()!!.size}" }
    }

    @Test
    fun getUser_avatar() {
        mockResponse("GithubUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!![12].avatarUrl
                == "https://avatars3.githubusercontent.com/u/22?v=4")
    }

    @Test
    fun getUser_username() {
        mockResponse("GithubUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!![12].username == "macournoyer")
    }


    fun mockResponse(filename: String) {
        val inputStreamReader = InputStreamReader(javaClass.classLoader.getResourceAsStream(filename))
        mockWebServer.enqueue(
                MockResponse().setBody(
                        BufferedReader(inputStreamReader)
                                .lines()
                                .collect(Collectors.joining("\n"))
                )
        )
    }
}