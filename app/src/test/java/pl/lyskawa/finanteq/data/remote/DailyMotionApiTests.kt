package pl.lyskawa.finanteq.data.remote

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

@RunWith(JUnit4::class)
class DailyMotionApiTests {

    private lateinit var service: DailyMotionApi

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(DailyMotionApi::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getUser_notNull() {
        mockResponse("DailyMotionUsers.json")

        val users = service.getUsers().execute()

        assert(users.body() != null)
    }


    @Test
    fun getUser_notEmpty() {
        mockResponse("DailyMotionUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!!.list.isNotEmpty())
    }

    @Test
    fun getUser_size() {
        mockResponse("DailyMotionUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!!.list.size == 10) { "correct size is ${users.body()!!.list.size}" }
    }

    @Test
    fun getUser_avatar() {
        mockResponse("DailyMotionUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!!.list[9].avatarUrl == "http://s1.dmcdn.net/AVM/360x360-png/iC2.jpg")
    }

    @Test
    fun getUser_username() {
        mockResponse("DailyMotionUsers.json")

        val users = service.getUsers().execute()

        assert(users.body()!!.list[9].username == "farahtanzeel061")
    }


    fun mockResponse(filename: String) {
        val inputStreamReader = InputStreamReader(javaClass.classLoader.getResourceAsStream(filename))
        mockWebServer.enqueue(
                MockResponse().setBody(
                        BufferedReader(inputStreamReader)
                                .lines()
                                .collect(Collectors.joining("\n"))
                )
        )
    }
}