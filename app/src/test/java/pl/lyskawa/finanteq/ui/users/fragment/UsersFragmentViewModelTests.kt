package pl.lyskawa.finanteq.ui.users.fragment


import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import pl.lyskawa.finanteq.data.model.User
import pl.lyskawa.finanteq.data.model.UserSource
import pl.lyskawa.finanteq.data.remote.DailyMotionApi
import pl.lyskawa.finanteq.data.remote.GithubApi

@RunWith(JUnit4::class)
class UsersFragmentViewModelTests {

    @Rule
    @JvmField
    var rule = InstantTaskExecutorRule()

    private val githubApi = mock(GithubApi::class.java)
    private val dailyMotionApi = mock(DailyMotionApi::class.java)
    private val usersFragmentViewModel = UsersFragmentViewModel(githubApi, dailyMotionApi)

    @Test
    fun githubUsersFetched() {
        val users = listOf(User(username = "name", avatarUrl = "url", source = UserSource.GITHUB))
        val observer = mock<Observer<List<User>>>()
        usersFragmentViewModel.users.observeForever(observer)
        usersFragmentViewModel.onDataFetched(users, UserSource.GITHUB)
        verify(observer, never()).onChanged(any())
    }

    @Test
    fun dailymotionUsersFetched() {
        val users = listOf(User(username = "name", avatarUrl = "url", source = UserSource.GITHUB))
        val observer = mock<Observer<List<User>>>()
        usersFragmentViewModel.users.observeForever(observer)
        usersFragmentViewModel.onDataFetched(users, UserSource.DAILYMOTION)
        verify(observer, never()).onChanged(any())
    }


}