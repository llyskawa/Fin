
import org.junit.runner.RunWith
import org.junit.runners.Suite
import pl.lyskawa.finanteq.data.remote.DailyMotionApiTests
import pl.lyskawa.finanteq.data.remote.GithubApiTests

@RunWith(Suite::class)
@Suite.SuiteClasses(GithubApiTests::class, DailyMotionApiTests::class)
class ApiTestSuite