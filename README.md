# Todo

- [x] Architecture
    - [x] Dagger 2 injection
    - [x] LiveData
    - [x] Retrofit interfaces
    - [x] Databinding w/ Binding Adapters 

- [x] Screens
    - [x] Users screen
    - [x] Users fragment
    - [x] Users list
    - [x] Details screen

- [x] Functions
    - [x] Fetch data from APIs
    - [x] Merge fetched data into single list
    - [x] Swipe to refresh

- [x] Tests
    - [x] Unit tests
    - [x] UI tests

- [ ] Extra
    - [x] Screen transitions
    - [x] Shared Content transitions
    - [ ] More extensive testing
    - [ ] GitLab CI
    - [ ] Paging

# Description

The app is fetching basic user information from Dailymotion and Github public APIs and shows a "details" view when a list item is clicked.

View hierarchy:
1. Users Activity
    - Users Fragment 
        - Users List (RecyclerView)
2. Detail Activity